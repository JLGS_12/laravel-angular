<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {
          JWTAuth::parseToken()->authenticate();
          return $next($request);
        } catch (\Exception $e) {
          return response()->json(['token_expired']);
        }
    }
}

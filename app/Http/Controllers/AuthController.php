<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\cuentos;
class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt:api', ['except' => ['login']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
     public function  cuentos(Request $req){
       $all = $req->all();
       $token = $req->reToken;
       if($this->isReCaptchaValid($token)){
         return response()->json(["pasaste"=>"broster"]);
       }
      return response()->json(["no paso"=>"broster"]);
     }

     public function guardarCuento(Request $req){
       $all = $req->all();
       $token = $req->reToken;
       if($this->isReCaptchaValid($token)){
         unset($all->reToken);
         $id  = auth('api')->user()->id;
         $res = cuentos::create([
           'id_user' => $id,
            'd_genero' => $all->genero,
            'd_titulo' => $all->titulo,
            'd_texto' => $all->texto,
            'd_resumen_texto' => $all->resumen_texto,
            'd_autor' => $all->autor,
            'd_dniAutor' => $all->dniAutor,
            'd_edad' => $all->edad,
            'd_fecha_referencia' => $all->fecha_referencia,
            'd_ciudad' => $all->ciudad,
            'd_nota' => $all->nota,
         ]);
         return response()->json(["pasaste"=>"Cuento Guardado"]);
       }
       return response()->json(["no paso"=>"broster"]);
     }
    public function me()
    {
      try {
        $ret  = auth('api')->user();
        $retornar = ["avatar"=>$ret->avatar,"name"=>$ret->name,"email"=>$ret->email];
        return response()->json($retornar);
      } catch (\Exception $e) {
        return response()->json(['token_expired'], $e->getStatusCode());
      }


    }
    public function payload()
    {
        return response()->json(auth('api')->payload());
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()->email,
        ]);
    }

    public function isReCaptchaValid($rcToken){

    $client = new Client([
        'base_uri' => 'https://google.com/recaptcha/api/'.config('recaptcha.reCaptchaSite'),
        'timeout' => 5.0
    ]);

    $response = $client->request('POST', 'siteverify', [
        'query' => [
            'secret' => config('recaptcha.reCaptchaSecretKey'),
            'response' => $rcToken
        ]
    ]);

    $data = json_decode($response->getBody());

    if($data->success && $data->score >= 0.5){

        return true;

    }

    return false;


}
}

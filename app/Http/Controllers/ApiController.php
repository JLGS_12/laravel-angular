<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    function dummy (Request $req){
        $a = ["hola"=>"mundo"];
        //return response()->json(\array_merge($a ,$req->all()) );
        return response()->json(\array_merge($a ,$req->all()) );
    }
    function dummyPost(Request $req){
        return response()->json(["holaMundo"=>"post"]);
    }
}

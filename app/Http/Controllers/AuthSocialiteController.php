<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthControllerSocialite;

class AuthSocialiteController extends Controller
{
    //

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
{
    return Socialite::driver('google')->redirect();
}

public function handleProviderCallback()
  {
      try {
          $user = Socialite::driver('google')->user();

      } catch (\Exception $e) {
          return redirect('/redirect');
      }

      $existingUser = User::where('email', $user->email)->first();
      if($existingUser){
          $token = app('App\Http\Controllers\AuthControllerSocialite')->login($existingUser);
          return redirect()->to(config('recaptcha.frontEndURL').'?logged='.$token->original['access_token']);
      } else {
          $user2 = User::create([
              'name' => $user->name,
              'lastName' => $user->name,
              'email' => $user->email,
              'google_id' => $user->id,//Hash::make('0000'),
              'avatar' => $user->avatar,
              'avatar_original' => $user->avatar_original,
          ]);
          $token = app('App\Http\Controllers\AuthControllerSocialite')->login($user2);
          return redirect()->to(config('recaptcha.frontEndURL').'?logged='.$token->original['access_token']);
      }
}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthControllerSocialite;
use Google_Client;
use Google_Service_Drive;

class Auth2Controller extends Controller
{
    //

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
{
    return Socialite::driver('google')->redirect();
}

public function handleProviderCallback(Request $req)
  {
    $token = $req->token;
    $client = new Google_Client(['client_id' => config('recaptcha.google_id')]);  // Specify the CLIENT_ID of the app that accesses the backend
    $payload = $client->verifyIdToken($token);
    if ($payload) {
      $userEmail = $payload['email'];
      $existingUser = User::where('email', $userEmail)->first();
      if($existingUser){
          return $this->retornar($existingUser);
      } else {
          $user2 = User::create([
              'name' => $payload['given_name'],
              'lastName' => $payload['family_name'],
              'email' => $payload['email'],
              'google_id' => $payload['sub'],//Hash::make('0000'),
              'avatar' => $payload['picture'],
              'avatar_original' => $payload['picture'],
          ]);
          return $this->retornar($user2);
      }
    } else {
      return response()->json('error');
    }

}

public function retornar ($user){
  $token = app('App\Http\Controllers\AuthControllerSocialite')->login($user);
  $retornar = [
    "token"=>$token,
    "data"=>["avatar"=>$user->avatar,
    "name"=>$user->name,
    "email"=>$user->email]
  ];
  return $retornar;
}

}

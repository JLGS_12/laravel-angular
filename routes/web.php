<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::post('cuentos', array('middleware' => 'cors', 'uses' => 'ApiController@dummy'));
// Route::post('cuentosPost', array('middleware' => 'cors', 'uses' => 'ApiController@dummyPost'));

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//SOCIALITE
Route::get('/redirect', 'AuthSocialiteController@redirectToProvider')->name('redirect');;
Route::get('/callback', 'AuthSocialiteController@handleProviderCallback');

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', array('middleware' => 'cors', 'uses' => 'AuthController@login'));
    Route::post('logout', array('middleware' => 'cors', 'uses' => 'AuthController@logout'));
    Route::post('refresh', array('middleware' => 'cors', 'uses' => 'AuthController@refresh'));
    Route::post('me', array('middleware' => 'cors', 'uses' => 'AuthController@me'));
    Route::post('payload', array('middleware' => 'cors', 'uses' => 'AuthController@payload'));
    Route::post('cuentos',array('middleware'=>'cors','uses'=>'AuthController@cuentos'));
    Route::post('guardar-cuentos',array('middleware'=>'cors','uses'=>'AuthController@cuentos'));
    Route::post('oauthGoogle', array('middleware' => 'cors', 'uses' => 'Auth2Controller@handleProviderCallback') );
});

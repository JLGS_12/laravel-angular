<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::truncate();
        $user = new User();
            $user->email = 'admin@admin.com';
            $user->password = Hash::make('adminadmin');
            $user->google_id = Hash::make('adminadmin');
            $user->name = 'Administrator';
            $user->lastName = 'Administrator';
        $user->save();
    }
}

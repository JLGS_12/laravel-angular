<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_user');
            $table->string('d_genero');
            $table->string('d_titulo');
            $table->string('d_texto');
            $table->string('d_resumen_texto');
            $table->string('d_autor');
            $table->string('d_dniAutor');
            $table->string('d_edad');
            $table->string('d_fecha_referencia');
            $table->string('d_ciudad');
            $table->string('d_nota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentos');
    }
}
